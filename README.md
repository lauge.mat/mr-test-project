# MR-test-project

We want a list of students and their corresponding groups


We want it in the form of

```

## Group Xn

* John Doe
* Mary Something
* Extra Name

```

## Group A1 

* Mathias Knudsen
* Rudi Hansen
* Jakob Dahl
* Mikkel Henriksen
* Lauge Mathiesen

## Group A2 

* Nicolai Larsen
* Oscar Harttung
* Benjamin Bom Christensen
* Manisha Gurung

## Group A3 
* Bogdan Robert
* Mark Christiansen 
* Andrei Romar
* Jacob Berg Koch

## Group A4

## Group B1 
* Jonas Binti Jensen

## Group B2 
* Nikolaj Hult-Christensen  
* Alexander Bjørk Andersen
* Aleksis Kairiss
* Jacob Suurballe Petersen
* Dainty Lyka Bihay Olsen
* Daniel Rasmussen


## Group B3 
* Aubrey Jones
* Henrik Holm Hansen
* Aleksandra Voronina
* Camilla Errendal


## Group B4

* Ulrik Vinther-Jensen
* Mathias Andersen
* Bogdan-Alexandru Vanghelie
